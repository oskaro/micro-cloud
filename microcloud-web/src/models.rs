use std::path::Path;

#[derive(Queryable)]
pub struct User {
    id: i32,
    username: String,
    email: String,
}
