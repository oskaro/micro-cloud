use actix_multipart::Multipart;
use actix_web::{get, middleware::Logger, post, web, App, Error, HttpResponse, HttpServer};
use env_logger;
use futures_util::TryStreamExt as _;
use log::{debug, info};
use std::io::{Seek, SeekFrom, Write};
use std::path::Path;
use std::sync::Arc;
use std::time::Instant;
use tempfile;

use microcloud_backends::FileSystemBackend;
use microcloud_indexer::PostgresIndexer;
use microcloud_lockers::RedisLocker;
use microcloud_storage::file_storage::FileStorage;

#[macro_use]
extern crate diesel;
extern crate dotenv;

mod datastore;
mod models;
mod schema;

#[derive(Clone)]
struct State {
    store: Arc<FileStorage<PostgresIndexer, FileSystemBackend, RedisLocker>>,
}

const TEMP_DIR: &str = "./tmp/";

/// Handle file uploads to the cloud. This method can upload 1GB/s on a local computer.
#[post("/upload")]
async fn upload(state: web::Data<State>, mut payload: Multipart) -> Result<HttpResponse, Error> {
    log::info!("Got new request");
    let start_time = Instant::now();
    while let Some(mut field) = payload.try_next().await? {
        // A multipart/form-data stream has to contain `content_disposition`
        debug!("Extracting content_disposition");
        let content_disposition = field
            .content_disposition()
            .ok_or_else(|| HttpResponse::BadRequest().finish())?;

        debug!("getting filename");
        let filename = content_disposition
            .get_filename()
            .and_then(|f| Some(sanitize_filename::sanitize(f)))
            .ok_or_else(|| HttpResponse::BadRequest().finish())?;

        let mut filepath = std::path::PathBuf::from(TEMP_DIR);
        filepath.push(filename);

        // File::create is blocking operation, use threadpool
        let mut f = web::block(|| tempfile::tempfile()).await?;

        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.try_next().await? {
            // filesystem operations are blocking, we have to use threadpool
            f = web::block(move || f.write_all(&chunk).map(|_| f)).await?;
        }

        f.seek(SeekFrom::Start(0))?;
        let bstate = state.clone();
        web::block(move || {
            bstate.store.insert_file(
                std::io::BufReader::new(f),
                std::path::Path::new(""),
                "oskaro".to_string(),
            )
        })
        .await
        .map_err(|e| HttpResponse::InternalServerError().body(e.to_string()))?;
    }

    let duration = start_time.elapsed().as_millis();

    Ok(HttpResponse::Ok().body(format!("{{\"duration\":{}}}", duration)))
}

#[get("/upload")]
fn index() -> HttpResponse {
    let html = r#"<html>
        <head><title>Upload Test</title></head>
        <body>
            <form target="/" method="post" enctype="multipart/form-data">
                <input type="file" multiple name="file"/>
                <button type="submit">Submit</button>
            </form>
        </body>
    </html>"#;

    HttpResponse::Ok().body(html)
}
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    log::info!("Logging initiated");

    let state = State {
        store: Arc::new(FileStorage::new(
            Path::new("/"),
            PostgresIndexer::new(
                std::env::var("POSTGRES_URI")
                    .expect("POSTGRES_URI env variable is required to connect to postgres"),
            ),
            FileSystemBackend::new(Path::new("/ucloud-data")),
            RedisLocker::new(
                std::env::var("REDIS_URI")
                    .expect("REDIS_URI is required to create a new redis locker"),
            ),
        )),
    };

    log::info!("State is initialized");
    HttpServer::new(move || {
        App::new()
            .data(state.clone())
            .wrap(Logger::default())
            .service(upload)
            .service(index)
    })
    .bind("127.0.0.1:8081")?
    .run()
    .await
}
