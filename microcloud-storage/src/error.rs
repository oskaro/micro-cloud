use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("could not perform IO operation: {0}")]
    Io(#[from] std::io::Error),
    #[error("error from storage backend: {0}")]
    StorageError(String),
    #[error("error from indexer: {0}")]
    IndexerError(String),
    #[error("error from file locker: {0}")]
    FileLockError(String),
}
