use crate::error::Error;
use crate::{FileLocker, Indexer, Result, StorageBackend};

use std::path::{Path, PathBuf};

// TODO Should the FileStorage at all be conserned with authentication?
/// The FileStorage brings the indexer and StorageBackend together.
pub struct FileStorage<I: Indexer, B: StorageBackend, L: FileLocker> {
    // TODO do we need to store the storage root?
    /// The base path is the folder where the FileStorage will store all its content.
    storage_root: PathBuf,
    /// The indexer is used to keep track of files in the file system in the backend
    indexer: I,
    /// The storage backend is used to store the files to physical medium
    backend: B,
    locker: L,
}

impl<I, B, L> FileStorage<I, B, L>
where
    I: Indexer,
    B: StorageBackend,
    L: FileLocker,
{
    /// Create a new file storage at the given base path
    pub fn new(
        storage_root: &Path,
        indexer: I,
        backend: B,
        locker: L,
    ) -> FileStorageBuilder<I, B, L> {
        FileStorageBuilder {
            storage_root: PathBuf::from(storage_root),
            indexer,
            backend,
            locker,
        }
    }

    fn create_file_path(&self, file: &Path) -> PathBuf {
        let mut path = self.storage_root.clone();
        path.push(file);
        path
    }

    /// Add a file to the file index and add it to the physical storage
    /// The file to be added must be available at the path given.
    pub fn insert_file(&mut self, file: &Path, dest: &Path) -> Result<()> {
        let dest = &self.create_file_path(dest);
        if self.indexer.file_exists(dest)? {
            return Err(Error::IndexerError("File alreadt exists".into()));
        }

        if !self.locker.try_lock(&dest)? {
            return Err(Error::FileLockError(
                "could not lock file, it is already locked".into(),
            ));
        }
        match self.indexer.add_file(dest) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("{}", e);
                self.locker.unlock(dest)?;
                return Err(e);
            }
        }

        self.backend.store_file(file, dest).map_err(|e| {
            self.locker
                .unlock(dest)
                .unwrap_or_else(|err| eprintln!("{}", err));
            e
        })?;

        Ok(())
    }

    pub fn delete_file(&mut self, file: &Path) -> Result<()> {
        let file = &self.create_file_path(file);
        self.locker.try_lock(&file)?;

        let delete = self.backend.delete_file(&file);
        self.locker.unlock(&file)?;

        delete?;
        Ok(())
    }
}

pub struct FileStorageBuilder<I: Indexer, B: StorageBackend, L: FileLocker> {
    storage_root: PathBuf,
    indexer: I,
    backend: B,
    locker: L,
}

impl<I: Indexer, B: StorageBackend, L: FileLocker> FileStorageBuilder<I, B, L>
where
    I: Indexer,
    B: StorageBackend,
{
    pub fn new(&mut self, storage_root: &Path, indexer: I, backend: B, locker: L) -> Self {
        Self {
            storage_root: storage_root.to_path_buf(),
            indexer,
            locker,
            backend,
        }
    }

    pub fn storage_root(&mut self, storage_root: &Path) -> &mut Self {
        self.storage_root = PathBuf::from(storage_root);
        self
    }

    pub fn indexer(&mut self, indexer: I) -> &mut Self {
        self.indexer = indexer;
        self
    }

    pub fn backend(&mut self, backend: B) -> &mut Self {
        self.backend = backend;
        self
    }

    pub fn build(self) -> Result<FileStorage<I, B, L>> {
        Ok(FileStorage {
            indexer: self.indexer,
            backend: self.backend,
            locker: self.locker,
            storage_root: self.storage_root,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Filter;
    use crate::Result;
    use std::collections::{HashMap, HashSet};
    use std::env;
    use tempfile;
    // Stolen from: https://andrewra.dev/2019/03/01/testing-in-rust-temporary-files/
    macro_rules! in_temp_dir {
        ($block:block) => {
            let tmpdir = tempfile::tempdir().unwrap();
            env::set_current_dir(&tmpdir).unwrap();

            $block;
        };
    }
    fn create_file_path(file: &Path) -> String {
        let mut path = PathBuf::from("/");
        path.push(file);

        path.to_string_lossy().to_string()
    }
    struct MockBackend {
        // Filenames currently in the storage
        storage: HashSet<String>,
    }

    impl MockBackend {
        pub fn new() -> Self {
            Self {
                storage: HashSet::new(),
            }
        }
    }

    struct MockIndexer {
        // Filename, if the file is locked and other info
        index: HashMap<String, bool>,
    }

    impl MockIndexer {
        pub fn new() -> Self {
            Self {
                index: HashMap::new(),
            }
        }
    }

    struct MockLocker {
        locked_files: HashSet<String>,
    }

    impl MockLocker {
        pub fn new() -> Self {
            Self {
                locked_files: HashSet::new(),
            }
        }
    }

    impl StorageBackend for MockBackend {
        fn store_file(&mut self, _source: &Path, destination: &Path) -> Result<()> {
            let dest = destination.to_string_lossy();
            if self.storage.insert(dest.to_string()) {
                Ok(())
            } else {
                Err(Error::StorageError("".into()))
            }
        }

        fn get_file(&self, path: &Path) -> Result<()> {
            let path = path.to_string_lossy().to_string();
            if self.storage.contains(&path) {
                Ok(())
            } else {
                Err(Error::StorageError(String::from("file does not exist.")))
            }
        }

        fn delete_file(&mut self, path: &Path) -> Result<()> {
            let path = path.to_string_lossy().to_string();
            if self.storage.remove(&path) {
                Ok(())
            } else {
                Err(Error::StorageError("".into()))
            }
        }

        fn move_file(&mut self, source: &Path, destination: &Path) -> Result<()> {
            let path = source.to_string_lossy().to_string();
            let dest = destination.to_string_lossy().to_string();
            if self.storage.contains(&path) {
                // The source exists, now lets try to move the file.
                if self.storage.insert(dest) {
                    self.storage.remove(&path);
                    return Ok(());
                } else {
                    // Destination already exists
                    return Err(Error::StorageError("".into()));
                }
            } else {
                return Err(Error::StorageError("".into()));
            }
        }
    }

    impl Indexer for MockIndexer {
        fn add_file(&mut self, file: &Path) -> Result<()> {
            let path = create_file_path(file);

            if !self.index.contains_key(&path) {
                self.index.insert(path, false);
                Ok(())
            } else {
                Err(Error::IndexerError("".into()))
            }
        }

        fn find_files_by(&self, _filter: Filter) -> Result<Option<()>> {
            Ok(None)
        }

        fn file_exists(&self, file: &Path) -> Result<bool> {
            let path = create_file_path(file);

            Ok(self.index.contains_key(&path))
        }

        fn remove_file(&mut self, file: &Path) -> Result<()> {
            let path = create_file_path(file);
            if self.index.contains_key(&path) {
                Ok(())
            } else {
                Err(Error::IndexerError("".into()))
            }
        }

        fn move_file(&mut self, source: &Path, destination: &Path) -> Result<()> {
            let source = create_file_path(source);
            let dest = create_file_path(destination);

            if self.index.contains_key(&source) {
                if self.index.contains_key(&dest) {
                    Ok(())
                } else {
                    Err(Error::IndexerError("".into()))
                }
            } else {
                Err(Error::IndexerError("".into()))
            }
        }
    }

    impl FileLocker for MockLocker {
        fn try_lock(&mut self, file: &Path) -> Result<bool> {
            Ok(!self.is_locked(file))
        }

        /// Unlock a file, the user must make sure that the file locked is not locked by another
        /// instance.
        fn unlock(&mut self, file: &Path) -> Result<()> {
            self.locked_files
                .remove(&file.to_string_lossy().to_string());
            Ok(())
        }

        /// Check to see if a file is locked.
        fn is_locked(&self, file: &Path) -> bool {
            self.locked_files
                .contains(&file.to_string_lossy().to_string())
        }
    }

    fn create_new_file_storage(path: &Path) -> FileStorage<MockIndexer, MockBackend, MockLocker> {
        let b = MockBackend::new();
        let i = MockIndexer::new();
        let l = MockLocker::new();
        FileStorage::new(path, i, b, l).build().unwrap()
    }

    #[test]
    fn it_works() {
        let _ = create_new_file_storage(Path::new("/tmp/"));
    }

    #[test]
    fn store_a_file() {
        in_temp_dir!({
            let mut fs = create_new_file_storage(Path::new("/tmp/"));

            let new_file = Path::new("./blah_blah");
            let f = tempfile::tempfile().unwrap();

            fs.insert_file(&new_file, &new_file).unwrap();

            assert!(fs.get_file(&new_file))
        });
    }
}
