use std::path::Path;

pub mod error;
pub mod file_storage;

use error::Error;
pub type Result<T> = std::result::Result<T, Error>;

pub enum Filter {
    Extension(String),
    Name(String),
    BasePath(Box<Path>),
}

// TODO this interface is getting quite large, maybe it should be two traits? One for locking and
// one for file indexing.
// TODO does the indexer need to keep track of the users? It probably should be able to search by
// user, though.
/// An indexer keeps track of files for the file storage. It must be able to lock files for
/// editing, insert new files and find files by some attribute.
/// An indexer may be a database, local storage or any other means.
pub trait Indexer {
    fn add_file(&mut self, file: &Path) -> Result<()>;
    fn find_files_by(&self, filter: Filter) -> Result<Option<()>>;
    fn file_exists(&self, file: &Path) -> Result<bool>;
    fn remove_file(&mut self, file: &Path) -> Result<()>;
    fn move_file(&mut self, source: &Path, destination: &Path) -> Result<()>;
}

/// A FileLocker is responsible for locking files so that no other storage backend can change that
/// file until it is released
pub trait FileLocker {
    // TODO might try to implement locking with a guard struct of some kind, but that might be
    // premature optimization/too complex.
    /// Try to lock a given file
    fn try_lock(&mut self, file: &Path) -> Result<bool>;

    /// Unlock a file, the user must make sure that the file locked is not locked by another
    /// instance.
    fn unlock(&mut self, file: &Path) -> Result<()>;

    /// Check to see if a file is locked.
    fn is_locked(&self, file: &Path) -> bool;
}

/// A StorageBackend implements a way to move files into a storage back end, like local storage, S3
/// or other cloud storage.
pub trait StorageBackend {
    /// Takes the source file and move it to the destination on the storage backend.
    fn store_file(&mut self, source: &Path, destination: &Path) -> Result<()>;
    fn move_file(&mut self, source: &Path, destination: &Path) -> Result<()>;
    fn delete_file(&mut self, path: &Path) -> Result<()>;
    fn get_file(&self, path: &Path) -> Result<()>;
}
